This project was created with [Create React App TypeScript](https://github.com/wmonk/create-react-app-typescript).

Demo: https://willyfitrahendria.gitlab.io/simple-todo-list-react (**Under Development**)

## How to start the app

- clone and navigate to the project directory
- npm install
- npm start