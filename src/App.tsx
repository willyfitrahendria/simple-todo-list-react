import * as React from 'react';
import './App.css';
import Header from './components/Header';
import TodoList from './components/TodoList';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Header />
        <section className="App-section">
          <TodoList />
        </section>
      </div>
    );
  }
}

export default App;
