import * as React from 'react';
import logo from './../logo.svg';

class Header extends React.Component {
  public render() {
    return (
      <header className="App-header">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Simple To-do List App</h1>
        <h5 className="App-subtitle">(React + TypeScript + Material UI)</h5>
      </header>
    );
  }
}

export default Header;
