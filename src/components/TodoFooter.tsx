import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import * as React from 'react';
import {noBorderRadius} from './../constants/constants';
import './TodoFooter.css';

class TodoFooter extends React.Component {
	public render() {
		return (
			<div className="todo-footer">
				<Card style={noBorderRadius}>
					<span className="todo-remaining">1 item left</span>
					<Button className="show-all todo-filter" color="primary" size="small" variant="outlined">All</Button>
					<Button className="show-active todo-filter" color="primary" size="small">Active</Button>
					<Button className="show-complete todo-filter" color="primary" size="small">Completed</Button>
					<Button style={noBorderRadius} className="remove-all" variant="contained" color="secondary">Remove All</Button>
				</Card>
			</div>
		);
	}
}

export default TodoFooter;