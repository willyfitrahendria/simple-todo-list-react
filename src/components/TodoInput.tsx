import Card from '@material-ui/core/Card';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import * as React from 'react';
import {noBorderRadius} from './../constants/constants';
import './TodoInput.css';

class TodoInput extends React.Component {
	public render() {
		return (
			<div className="todo-input">
				<Card style={noBorderRadius}>
					<Checkbox className="select-all"/>
					<Input className="todo-text-input"/> 
				</Card>
			</div>
		);
	}
}

export default TodoInput;