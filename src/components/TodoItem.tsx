import Card from '@material-ui/core/Card';
import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import * as React from 'react';
import {noBorderRadius} from './../constants/constants';
import './TodoItem.css';

class TodoItem extends React.Component {
	public render() {
		return (
			<div className="todo-item">
				<Card style={noBorderRadius}>
					<Checkbox className="todo-check"/>
					<span className="todo-title">This is title.</span>
					<IconButton className="todo-delete" color="secondary">
						<Icon>close</Icon>
					</IconButton>
				</Card>
			</div>
		);
	}
}

export default TodoItem;