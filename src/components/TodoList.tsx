import * as React from 'react';
import TodoFooter from './TodoFooter';
import TodoInput from './TodoInput';
import TodoItem from './TodoItem';
import './TodoList.css';

class TodoList extends React.Component {
	public render() {
		// const { completed = false, editing = false, title } = this.props;
		return (
			<div className="todo-list-component">
				<TodoInput />
				<TodoItem />
				<TodoFooter />
			</div>
		);
	}

}

export default TodoList;